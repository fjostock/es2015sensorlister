package de.rub.rus.sensorlister;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView sensorList;
    private TextView accelerometerInfoText, rotationVectorInfoText;
    // TODO: delete if obsolete
    //        thermometerInfoText, gravitySensorInfoText;
    // private TextView gyroscopeSensorInfoText, heartRateSensorInfoText;
    private SensorManager theSensorManager;
    private Button accelerometerInfoButton, accelerometerRunSensorButton;
    private Button rotationVectorInfoButton, rotationVectorRunSensorButton;
    // TODO: delete if obsolete
    // private Button thermometerInfoButton, thermometerRunSensorButton;
    //private Button gravitySensorInfoButton, gravitySensorRunSensorButton;
    //private Button gyroscopeSensorInfoButton, gyroscopeSensorRunSensorButton;
    //private Button heartRateSensorInfoButton, heartRateSensorRunSensorButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        identifyButtonsAndTextViews();

        theSensorManager= (SensorManager) this.getSystemService(this.SENSOR_SERVICE);

        checkSensors();
    }

    /**
     Assigns all info button, run sensor button and info text views (for example,
     accelerometerInfoButton, accelerometerRunSensorButton and accelerometerInfoText)
     to local variables; wrapper method required to keep things neat only.
    */
    private void identifyButtonsAndTextViews() {
        accelerometerInfoButton = (Button) findViewById(R.id.accelerometerInfoButton);
        accelerometerRunSensorButton= (Button) findViewById(R.id.accelerometerRunSensorButton);
        accelerometerInfoText = (TextView) findViewById(R.id.accelerometerSensorInfo);

        rotationVectorInfoButton= (Button) findViewById(R.id.rotationVectorSensorInfoButton);
        rotationVectorRunSensorButton= (Button) findViewById(R.id.rotationVectorSensorRunSensorButton);
        rotationVectorInfoText= (TextView) findViewById(R.id.rotationVectorSensorInfo);

        // TODO delete if obsolete
        //thermometerInfoButton= (Button) findViewById(R.id.ambientTemperatureInfoButton);
        //thermometerRunSensorButton= (Button) findViewById(R.id.ambientTemperatureRunSensorButton);
        //thermometerInfoText= (TextView) findViewById(R.id.ambientTemperatureSensorInfo);

        //gravitySensorInfoButton = (Button) findViewById(R.id.gravitySensorInfoButton);
        //gravitySensorRunSensorButton = (Button) findViewById(R.id.gravitySensorRunSensorButton);
        //gravitySensorInfoText = (TextView) findViewById(R.id.gravitySensorInfo);

        //gyroscopeSensorInfoButton = (Button) findViewById(R.id.gyroscopeSensorInfoButton);
        //gyroscopeSensorRunSensorButton = (Button) findViewById(R.id.gyroscopeSensorRunSensorButton);
        //gyroscopeSensorInfoText= (TextView) findViewById(R.id.gyroscopeSensorInfo);

        //heartRateSensorInfoButton = (Button) findViewById(R.id.heartRateSensorInfoButton);
        //heartRateSensorRunSensorButton = (Button) findViewById(R.id.heartRateSensorRunSensorButton);
        //heartRateSensorInfoText= (TextView) findViewById(R.id.heartRateSensorInfo);
    }

    /**
    Checks if sensor of type passed in 1st parameter exists and sets info text to yes/no
    and buttons to enabled/disabled accordingly.
     */
    private void checkSensor(int sensorType, TextView infoText, Button infoButton, Button runSensorButton){
        if (hasSensor(sensorType)) {
            infoText.setText("yes");
            infoButton.setEnabled(true);
            runSensorButton.setEnabled(true);
        } else {
            infoText.setText("no");
            infoButton.setEnabled(false);
            runSensorButton.setEnabled(false);
        }
    }

    /**
    Runs checkSensor for all sensors.
     */
    private void checkSensors(){

        TextView theTextView;

        checkSensor(Sensor.TYPE_ACCELEROMETER, accelerometerInfoText, accelerometerInfoButton, accelerometerRunSensorButton);

        // TODO: replace by call to checkSensor() after implementing corresponding classes
        // see Embedded Systems exercise
        theTextView= (TextView) findViewById(R.id.ambientTemperatureSensorInfo);
        if (hasSensor(Sensor.TYPE_AMBIENT_TEMPERATURE)) theTextView.setText("yes");
        findViewById(R.id.ambientTemperatureInfoButton).setEnabled(false);
        findViewById(R.id.ambientTemperatureRunSensorButton).setEnabled(false);

        // TODO: replace by call to checkSensor() after implementing corresponding classes
        // see Embedded Systems exercise
        theTextView= (TextView) findViewById(R.id.gravitySensorInfo);
        if (hasSensor(Sensor.TYPE_GRAVITY)) theTextView.setText("yes");
        findViewById(R.id.gravitySensorInfoButton).setEnabled(false);
        findViewById(R.id.gravitySensorRunSensorButton).setEnabled(false);

        // TODO: replace by call to checkSensor() after implementing corresponding classes
        // see Embedded Systems exercise
        theTextView= (TextView) findViewById(R.id.gyroscopeSensorInfo);
        if (hasSensor(Sensor.TYPE_GYROSCOPE)) theTextView.setText("yes");
        findViewById(R.id.gyroscopeSensorInfoButton).setEnabled(false);
        findViewById(R.id.gyroscopeSensorRunSensorButton).setEnabled(false);

        // TODO: replace by call to checkSensor() after implementing corresponding classes
        // see Embedded Systems exercise
        theTextView= (TextView) findViewById(R.id.heartRateSensorInfo);
        if (hasSensor(Sensor.TYPE_HEART_RATE)) theTextView.setText("yes");
        findViewById(R.id.heartRateSensorInfoButton).setEnabled(false);
        findViewById(R.id.heartRateSensorRunSensorButton).setEnabled(false);

        // TODO: replace by call to checkSensor() after implementing corresponding classes
        // see Embedded Systems exercise
        theTextView= (TextView) findViewById(R.id.lightSensorInfo);
        if (hasSensor(Sensor.TYPE_LIGHT)) theTextView.setText("yes");
        findViewById(R.id.lightSensorInfoButton).setEnabled(false);
        findViewById(R.id.lightSensorRunSensorButton).setEnabled(false);

        // TODO: replace by call to checkSensor() after implementing corresponding classes
        // see Embedded Systems exercise
        theTextView= (TextView) findViewById(R.id.linearAccelerationSensorInfo);
        if (hasSensor(Sensor.TYPE_LINEAR_ACCELERATION)) theTextView.setText("yes");
        findViewById(R.id.linearAccelerationSensorInfoButton).setEnabled(false);
        findViewById(R.id.linearAccelerationSensorRunSensorButton).setEnabled(false);

        // TODO: replace by call to checkSensor() after implementing corresponding classes
        // see Embedded Systems exercise
        theTextView= (TextView) findViewById(R.id.magneticFieldSensorInfo);
        if (hasSensor(Sensor.TYPE_MAGNETIC_FIELD)) theTextView.setText("yes");
        findViewById(R.id.magneticFieldSensorInfoButton).setEnabled(false);
        findViewById(R.id.magneticFieldSensorRunSensorButton).setEnabled(false);

        // TODO: replace by call to checkSensor() after implementing corresponding classes
        // see Embedded Systems exercise
        theTextView= (TextView) findViewById(R.id.pressureSensorInfo);
        if (hasSensor(Sensor.TYPE_PRESSURE)) theTextView.setText("yes");
        findViewById(R.id.pressureSensorInfoButton).setEnabled(false);
        findViewById(R.id.pressureSensorRunSensorButton).setEnabled(false);

        // TODO: replace by call to checkSensor() after implementing corresponding classes
        // see Embedded Systems exercise
        theTextView= (TextView) findViewById(R.id.proximitySensorInfo);
        if (hasSensor(Sensor.TYPE_PROXIMITY)) theTextView.setText("yes");
        findViewById(R.id.proximitySensorInfoButton).setEnabled(false);
        findViewById(R.id.proximitySensorRunSensorButton).setEnabled(false);

        // TODO: replace by call to checkSensor() after implementing corresponding classes
        // see Embedded Systems exercise
        theTextView= (TextView) findViewById(R.id.relativeHumiditySensorInfo);
        if (hasSensor(Sensor.TYPE_RELATIVE_HUMIDITY)) theTextView.setText("yes");
        findViewById(R.id.relativeHumiditySensorInfoButton).setEnabled(false);
        findViewById(R.id.relativeHumiditySensorRunSensorButton).setEnabled(false);

        checkSensor(Sensor.TYPE_ROTATION_VECTOR, rotationVectorInfoText, rotationVectorInfoButton, rotationVectorRunSensorButton);

    }


    /**
     * Checks if the sensor of the type passed in the first parameter exists.
     */
    private boolean hasSensor(int type){
        if (theSensorManager.getDefaultSensor(type)!=null){
          return(true);
        } else {
            return(false);
        }
    }

    /**
     * Method invoked when respective button is pushed.
     */
    public void sendGyroscopeRunSensorButtonPushed(View view){
        Intent theIntent= new Intent(this, RunGyroscopeSensorActivity.class);
        startActivity(theIntent);
    }

     /**
     * Method invoked when respective button is pushed.
     */
    public void sendAccelerometerRunSensorButtonPushed(View view){
        Intent theIntent= new Intent(this, RunAccelerometerSensorActivity.class);
        startActivity(theIntent);
    }

     /**
     * Method invoked when respective button is pushed.
     */
     // TODO: check which button this refers to by running the app
    public void sendRotationVectorSensorDetailsButtonPushed(View view) {
        Intent theIntent= new Intent(this, RunRotationVectorSensorActivity.class);
        startActivity(theIntent);
    }

     /**
     * Method invoked when respective button is pushed.
     */
    public void sendRotationVectorInfoButtonPushed(View view){
        Intent theIntent= new Intent(this, ShowRotationVectorSensorInfoActivity.class);
        startActivity(theIntent);
    }

     /**
     * Method invoked when respective button is pushed.
     */
    public void sendAccelerometerInfoButtonPushed(View view){
        Intent theIntent= new Intent(this, ShowAccelerometerSensorInfoActivity.class);
        startActivity(theIntent);
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */
}
